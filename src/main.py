import time
import os
import sys
import pygame
import random
import numpy as np

BASE_DIR = os.path.dirname(os.getcwd())
# IMAGES
# print()
RED_SHIP = pygame.image.load(os.path.join(BASE_DIR, "assets", "pixel_ship_red_small.png"))
RED_LASER = pygame.image.load(os.path.join(BASE_DIR, "assets", "pixel_laser_red.png"))
BLUE_SHIP = pygame.image.load(os.path.join(BASE_DIR, "assets", "pixel_ship_blue_small.png"))
BLUE_LASER = pygame.image.load(os.path.join(BASE_DIR, "assets", "pixel_laser_blue.png"))
GREEN_SHIP = pygame.image.load(os.path.join(BASE_DIR, "assets", "pixel_ship_green_small.png"))
GREEN_LASER = pygame.image.load(os.path.join(BASE_DIR, "assets", "pixel_laser_green.png"))
PLAYER_SHIP = pygame.image.load(os.path.join(BASE_DIR, "assets", "pixel_ship_yellow.png"))
PLAYER_LASER = pygame.image.load(os.path.join(BASE_DIR, "assets", "pixel_laser_yellow.png"))

# CONSTS
pygame.font.init()
GAME_FONT = pygame.font.SysFont('Comic Sans MS', 30)
white = (255, 255, 255)
black = (0, 0, 0)
dims = (600, 800)
display: pygame.display = pygame.display.set_mode((dims[0], dims[1]))
clock = pygame.time.Clock()
FPS = 60
player_velocity = 5
enemy_velocity = 2
laser_velocity = 3
##

background = np.array(
    [[white if random.randint(0, 1000) > 999 else black for _ in range(dims[1])] for _ in range(dims[0])])


class Laser:
    def __init__(self, x, y, img):
        self.x = x
        self.y = y
        self.laser_img = img
        self.mask = pygame.mask.from_surface(self.laser_img)

    # negative for player, positive for enemies
    def move(self, vel):
        self.y += vel

    def draw(self, window):
        # print(self.x, self.y)
        window.blit(self.laser_img, (self.x, self.y))

    def off_screen(self):
        return not 0 < self.y < dims[1]

    def collision(self, obj):
        return collide(obj, self)

    # ENEMY
    def move_lasers(self, vel, obj):
        self.move(vel)
        if self.off_screen():
            return True
        elif self.collision(obj):
            obj.health -= 10
            return True
        return False


class Ship:
    COOLDOWN = 35

    def __init__(self, x, y, health):
        self.x = x
        self.y = y
        self.health = health
        self.ship_img = None
        self.laser_img = None
        self.lasers = []
        self.cooldown = 0

    def draw(self, window):
        # print(self.x, self.y)
        window.blit(self.ship_img, (self.x, self.y))

    # move_lasers for the enemy
    # def move_lasers(self, vel, obj):
    #     self.cooldown_check()
    #     for laser in self.lasers:
    #         laser.move(vel)
    #         if laser.off_screen():
    #             self.lasers.remove(laser)
    #         elif laser.collision(obj):
    #             obj.health -= 10
    #             self.lasers.remove(laser)

    def cooldown_check(self):
        if self.cooldown >= self.COOLDOWN:
            self.cooldown = 0
        else:
            self.cooldown += 1

    def shoot(self):
        self.cooldown_check()
        if self.cooldown == 0:
            laser = Laser(self.x - 15, self.y, self.laser_img)
            # self.lasers.append(laser)
            self.cooldown = 1
            return laser
        return None

    def get_width(self):
        return self.ship_img.get_width()

    def get_height(self):
        return self.ship_img.get_height()


class PlayerShip(Ship):
    def __init__(self, x, y, health):
        super().__init__(x, y, health)
        self.ship_img = PLAYER_SHIP
        self.laser_img = PLAYER_LASER
        self.mask = pygame.mask.from_surface(self.ship_img)
        self.max_health = health

    # move player laser
    def move_lasers(self, vel, objs):
        self.cooldown_check()
        for laser in self.lasers:
            laser.move(vel)
            if laser.off_screen():
                self.lasers.remove(laser)
            else:
                for obj in objs:
                    if laser.collision(obj):
                        objs.remove(obj)
                        self.lasers.remove(laser)

    def shoot(self):
        if self.cooldown == 0:
            laser = Laser(self.x, self.y, self.laser_img)
            self.lasers.append(laser)
            self.cooldown = 1

    def healthbar(self):
        pygame.draw.rect(display, (255, 0, 0),
                         (self.x, self.y + self.ship_img.get_height() + 10,
                          self.ship_img.get_width(), 10))
        pygame.draw.rect(display, (0, 255, 0),
                         (self.x, self.y + self.ship_img.get_height() + 10,
                          self.ship_img.get_width() * (self.health / self.max_health), 10))

    def draw(self, display):
        super().draw(display)
        for laser in self.lasers:
            laser.draw(display)
        self.healthbar()


class EnemyShip(Ship):
    COLOR_MAP = {
        "red": (RED_SHIP, RED_LASER),
        "blue": (BLUE_SHIP, BLUE_LASER),
        "green": (GREEN_SHIP, GREEN_LASER)

    }

    def __init__(self, x, y, color, health=100):
        super().__init__(x, y, health)
        self.ship_img, self.laser_img = self.COLOR_MAP[color]
        self.mask = pygame.mask.from_surface(self.ship_img)
        self.max_health = health

    def move(self, vel):
        self.y += vel


def collide(obj1, obj2):
    offset_x = obj2.x - obj1.x
    offset_y = obj2.y - obj1.y
    return obj1.mask.overlap(obj2.mask, (offset_x, offset_y)) is not None


def main():
    level = 0
    lives = 5
    wave_length = 5
    my_ship = PlayerShip(250, 675, 100)
    enemies = []
    active_lasers = []
    lost_count = 0
    lost = False
    alive = True

    def paint_screen():
        lives_label = GAME_FONT.render(f"Lives: {lives}", 1, (255, 255, 255))
        level_label = GAME_FONT.render(f"Level: {level}", 1, (255, 255, 255))
        surf = pygame.surfarray.make_surface(background)
        display.blit(surf, (0, 0))
        my_ship.draw(display)

        for ship in enemies:
            ship.draw(display)

        for laser in active_lasers:
            laser.draw(display)

        display.blit(lives_label, (10, 10))
        display.blit(level_label, (485, 10))

        if lost:
            loser = GAME_FONT.render(f"You Lost!!", 1, (255, 255, 255))
            display.blit(loser, (dims[0] // 2 - 50, dims[1] // 2 - 50))
        pygame.display.update()

    def setup_screen(lost):

        # i don't know why it works like that, using numpy inverses the matrix requires me to use columns...
        # last_col = background[:, dims[1] - 1]
        if not lost:
            for pix in range(dims[1] - 2, -1, -1):
                background[:, pix + 1] = background[:, pix]

            # ENDLESS SPACE
            background[:, 0] = [white if random.randint(0, 1000) > 999 else black for _ in
                                range(dims[0])]
        paint_screen()

    while alive:
        setup_screen(lost)
        if lives == 0 or my_ship.health <= 0:
            lost = True
            lost_count += 1
        if lost:
            if lost_count > FPS * 5:
                alive = False
            else:
                continue  # if the game is over stop drawing.
        if len(enemies) == 0:
            level += 1
            for i in range(wave_length):
                enemies.append(EnemyShip(random.randrange(50, dims[0] - 50), random.randrange(-1500, -100),
                                         random.choice(["red", "blue", "green"])))
            wave_length += level
        # MOVEMENT
        pressed_keys = pygame.key.get_pressed()
        if pressed_keys[pygame.K_UP] and my_ship.y - player_velocity > 0:  # UP
            my_ship.y -= player_velocity
        if pressed_keys[pygame.K_DOWN] and my_ship.y + player_velocity < dims[1] - my_ship.get_height() - 15:  # DOWN
            my_ship.y += player_velocity
        if pressed_keys[pygame.K_RIGHT] and my_ship.x + my_ship.get_height() < dims[0]:  # RIGHT
            my_ship.x += player_velocity
        if pressed_keys[pygame.K_LEFT] and my_ship.x - player_velocity > 0:  # LEFT
            my_ship.x -= player_velocity
        if pressed_keys[pygame.K_SPACE]:
            my_ship.shoot()

        for laser in active_lasers[:]:
            if laser.move_lasers(laser_velocity, my_ship):
                active_lasers.remove(laser)
        for enemy in enemies[:]:
            enemy.move(enemy_velocity)
            if random.randrange(0, 3) == 1:
                new_laser = enemy.shoot()
                if new_laser is not None:
                    active_lasers.append(new_laser)
            # enemy.move_lasers(laser_velocity, my_ship)
            if enemy.y + enemy.get_height() > dims[1]:
                lives -= 1
                enemies.remove(enemy)
            if collide(enemy, my_ship):
                my_ship.health -= 10
                enemies.remove(enemy)
        my_ship.move_lasers(-laser_velocity, enemies)

        clock.tick(FPS)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()


def main_menu():
    run = True
    title_font = GAME_FONT.render("Press the mouse left button to start...", 1, (255, 255, 255))

    while run:
        display.blit(title_font, (15, dims[1] // 2 - 25))
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                main()
    pygame.quit()
    sys.exit()


if __name__ == "__main__":
    # starting positions
    main_menu()

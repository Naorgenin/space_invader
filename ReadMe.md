A space invader game, playing with pygame!

an example gif:
![Demo gameplay](readme/demo.gif "Demo")

### Improvements

* Moving star background!
* Decouple laser from spaceship - a fired laser is its own entity


inspiration from techwithtim, check him out:

https://www.youtube.com/watch?v=Q-__8Xw9KTM&ab_channel=TechWithTim